#! /usr/bin/bash

set -euo pipefail

rm -rf profile_out
rm -f ThroughputTest*.log
rm *.png
rm *.svg
