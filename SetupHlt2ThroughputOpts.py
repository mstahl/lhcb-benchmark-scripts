import os, importlib, glob
from Gaudi.Configuration import importOptions

num_events     = int(os.environ['NUMEVTS'])
num_eventslots = int(os.environ['NUMEVTSLOTS'])
num_threads    = int(os.environ['NUMTHREADS'])
testDBKey      = str(os.environ['TESTDBKEY'])
FTDecoVer      = int(os.environ['FTDECOVER'])
filepath       = glob.glob(os.environ['FILE'])
optsfile       = str(os.environ['OPTSFILE'])

if '/' not in optsfile: #try to get it from environment
  opts = importlib.import_module("upgrade_options.{}".format(optsfile))
else: #for quick local tests
  import sys
  mymodule = optsfile.rsplit('/',1)
  sys.path.insert(0,mymodule[0])
  opts=importlib.import_module(mymodule[1])

# fix for empty list coming from the env variables
if filepath == ['']:
    filepath = []

print("###SETUPTHROUGHPUTOPTS###\nImporting {} and executing with numevents:{}, eventslots:{}, threads:{} , using TestFileDBKey {}, SciFi decoding version {}, and set files to: {}".format(optsfile,num_events,num_eventslots, num_threads, testDBKey, FTDecoVer, filepath))

opts.runTest(testDBKey, num_eventslots, num_threads, num_events, FTDecoVer, filepath)
