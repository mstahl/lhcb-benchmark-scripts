import sys
import os
import re
import numpy
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patches as patches

def linExtrapolate(p1, p2, x):
    '''given 2 points in x,y format and an x in between,
       returns the linear extrapolation of the corresponding y value'''
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    return p1[1] + dy/dx*(x-p1[0])

def extrapolate(points, x):
    '''Given a set of points in (x,y) format, ordered in x,
       and an x not in tthe set, returns an extrapolated value
       for that x'''
    if x > points[-1][0]:
        return linExtrapolate(points[-2], points[-1], x)
    else:
        n = 0
        while x > points[n][0]:
            n += 1
        return linExtrapolate(points[n-1], points[n], x)

def extrapolatePoints(points, xs):
    '''points must be a set of points in (x,y) format ordered by x
       xs is a set of x locations also ordered by x
       The functions returns a list of y positions that contains
       the ones in points plus extrapolated values for the missing xs'''
    pointsD = dict(points)
    res = []
    for x in xs:
        if x in pointsD:
            res.append(pointsD[x])
        else:
            res.append(extrapolate(points, x))
    return res

def smoothEnvelop(xs, ys):
    '''Smoothes an anvelop by removing points where
       the value is too far from the linear extrapolation of the neighbours
       Note that input points are given in 2 vectors of x and y
       that must have the same len and xs must be sorted and with
       no redundancy. The ys list will be modified situ'''
    for n in range(1, len(xs)-1):
        # if going up, we keep the point
        if ys[n] > ys[n-1]:
            continue
        # compute extrapolation of previous and next point
        d2x = xs[n+1]-xs[n-1]
        d2y = ys[n+1]-ys[n-1]
        dx = xs[n]-xs[n-1]
        yexp = ys[n-1] + d2y/d2x*dx
        if (ys[n]-yexp)/abs(d2y) < -0.4:
            ys[n] = yexp

def getEnvelop(points):
    '''Computing top envelop of a scattered distribution
       points must be a list of couples x,y
       output is a pair : sorted lists of xs, sorted list of ys
       drawing the top envelop'''
    xSet = sorted(list(set([x for (x,y) in points])))
    # compute first naive envelop
    minY = min([y for (x,y) in points])
    top = {x:minY for x in xSet}
    for x,y in points:
        if y > top[x]:
            top[x] = y
    xtop, ytop = map(list, zip(*sorted(top.items())))
    # smooth it for "missing" points
    smoothEnvelop(xtop, ytop)
    # build
    return xtop, ytop


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--directory', default='./', type=str,
                        help='Directory containing input files')

    args  = parser.parse_args()

    files = [os.path.join(args.directory,f) for f in os.listdir(args.directory) if 'ThroughputTest_Scaling_' in f]
    regex = re.compile("Evts\/s = ([\d.]+)")

    content = {}

    for f in files:
        with open(f) as of:

            tmp = f.split('/')[-1].split('.')
            t = int(tmp[2][:-1])
            j = int(tmp[3][:-1])
            e = int(tmp[4][:-1])
            i = int(tmp[5])
            key = (t,j,e)
            lines = of.readlines()

            for l in lines[::-1]:
                tmp   = regex.search(l)
                if tmp:
                    if key in content:
                        content[key] += float(tmp.group(1))
                    else:
                        content[key] = float(tmp.group(1))

                    break


    # compute full list of points to display from the raw values
    # each point is a couple x, y and lists are indexed by nbThreads
    points = {}
    for (nt,nj,events), val in content.items():
        if nt not in points:
            points[nt] = []
        points[nt].append((nt*nj, val))
    # # get envelop
    xenv, yenv = getEnvelop(sum(points.values(), []))
    plt.plot(xenv, yenv, label='best hive perf', color='black')

    try:
        mjline = extrapolatePoints(sorted(points[1]), xenv)
        # # Setup colors
        #colors = dict(zip(sorted(points.keys()), cm.rainbow(numpy.linspace(0, 1, len(points.keys())))))
        # # plot and fill envelop, multi-job and non hive
        plt.plot(xenv, mjline, marker='o', label='Hive multijob', color=(.8,.8,.8))
        plt.fill_between(xenv, yenv, mjline, facecolor="none", hatch='\\\\', linewidth=0.0, label="MT gain")
    except:
        pass

    max_throughput = 0
    max_nt = -1
    max_j  = -1
    #xenv, yenv = getEnvelop(sum(pointsNH.values(), []))
    colors = dict(zip(sorted(points.keys()), cm.rainbow(numpy.linspace(0, 1, len(points.keys())))))
    # plot all dots

    for nt in sorted(points.keys()):
        if nt == 1:
            continue
        xps, yps = zip(*points[nt])
        if max(yps) > max_throughput:
            max_throughput = max(yps)
            max_nt = nt
            max_j  = xps[numpy.argmax(yps)]/nt
        plt.scatter(xps, yps, marker='o', label='%d thr/job' % nt, color=colors[nt], alpha=.7)

    print("Max reached throughput {} at {} jobs with {} threads".format(max_throughput,max_j, max_nt))

    # rework labels
    ax = plt.axes()
    plt.legend(loc=4, ncol=2)
    ax.grid(color=(.9,.9,.9))
    plt.xlabel('Level of parallelization (nb threads x nb jobs)')
    plt.ylabel('Events / s')
    plt.savefig('./scalingTest.png')

if __name__ == '__main__':
    sys.exit(main())
