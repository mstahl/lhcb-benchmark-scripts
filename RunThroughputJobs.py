#!/usr/bin/env python
"""Runs a single job as configured via specified options file which is passed to SetupThroughputOpts.py """

import re
import os
import sys
import argparse
import subprocess


def runJob(OutputFileName, jobidx, nbNumaNodes, modifiedEnv, doprofile):
    '''Run a test with the given options'''

    # open log file
    outputFile = open("{}.log".format(OutputFileName), 'w+')

    # build command line
    cmdLine = ["gaudirun.py", str(os.path.dirname(os.path.realpath(__file__))) + "/SetupThroughputOpts.py"]

    # deal with numa if needed
    if nbNumaNodes > 1:
        node = jobidx % nbNumaNodes
        cmdLine = ["numactl", "-N", str(node), "-m", str(node), "--"] + cmdLine

    # run the test
    print("Launching job with cmdLine: {}".format(cmdLine))
    process = subprocess.Popen(cmdLine, stdout=outputFile, stderr=subprocess.STDOUT, env=modifiedEnv)

    if doprofile:
        cmdLine = ["./doprofile.sh" , str(process.pid)]
        if nbNumaNodes > 1:
            node = (jobidx % nbNumaNodes) + 1
            cmdLine = ["numactl", "-N", str(node), "-m", str(node), "--"] + cmdLine
        profprocess = subprocess.Popen(cmdLine)


    return process, outputFile

def main():
    '''Main method : parses options and calls runJob which configures and runs a single Brunel job'''

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-j', '--jobs', type=int, required=True,
                        help='nb of jobs to launch')

    parser.add_argument('-t', '--threads', type=int, required=True,
                        help='nb of threads per job')

    parser.add_argument('-e', '--evtSlots', type=int, default=-1,
                        help='nb of event slots per job (default: nb of threads per job + 10%%)')

    parser.add_argument('-n', '--events', default=1000, type=int,
                        help='nb of events to process per thread (default: 1000)')

    parser.add_argument('--outfileTag', type=str, default='',
                        help="Output file name tag, name will be 'ThroughputTest_TAG_...' .(default: '')")

    parser.add_argument('OptsFileName', type=str,
                        help='Option file name which defines the reconstruction sequence to run.')

    parser.add_argument('TestFileDBKey', type=str,
                        help='TestFileDB key name which defines input files and tags.')

    parser.add_argument('--FTDecoVersion', default=4, type=int,
                        help='SciFi Decoding Version must match used input files (default: 4)')

    parser.add_argument('-f','--inputFileNames', type=str, nargs='+', default="",
                        help='Names of input files, multiple names possible')

    parser.add_argument('--nonuma', action='store_true',
                        help='whether to disable usage of numa domains.')

    parser.add_argument('--profile', action='store_true',
                        help='whether to enable vtune profiling - only supported for single job.')

    args = parser.parse_args()
    if args.evtSlots == -1:
        args.evtSlots = 1+int(11*args.threads/10)

    if args.profile and args.jobs >1:
        raise RuntimeError("VTune Profile only supported for single job")

    try:
        with open(os.devnull, 'w') as FNULL:
            subprocess.check_call(['amplxe-cl', '--help'], stdout=FNULL )
    except:
        if args.profile:
            raise RuntimeError("can't execute amplxe-cl, please make sure that you have intel tools setup correctly")
        else:
            print("Warning: can't execute amplxe-cl, please make sure that you have intel tools setup correctly")

    try:
        with open(os.devnull, 'w') as FNULL:
            subprocess.check_call(['gaudirun.py', '--help'], stdout=FNULL )
    except:
        raise RuntimeError("can't execute gaudirun.py, please make sure that you are in a Brunel environment")

    # deal with numa config
    nbNumaNodes = 1
    if not args.nonuma:
        try:
            output = subprocess.check_output(["numactl", "-show"])
            nodeline = [line for line in output.split('\n') if line.startswith('nodebind')][0]
            nbNumaNodes = len(nodeline.split()) - 1
            if(nbNumaNodes != args.jobs) and not args.profile:
                print("Warning: There are {} available numa nodes but you are launching {} jobs".format(nbNumaNodes, args.jobs))
        except:
            # numactl not existing
            print 'Warning : -- numactl not found, running without setting numa nodes --'


    # check how many threads the cpu actually has
    cputhreads = int(subprocess.check_output(["lscpu", "-e=CPU"]).split('\n')[-2]) + 1

    # check total number of threads
    if args.jobs*args.threads > cputhreads:
        print("CPU seems to only have {} threads but you specified {} jobs with {} threads each. This will overcommit the CPU, do you know what you are doing?".format(cputhreads, args.jobs, args.threads))

    myenv = os.environ.copy()
    myenv['NUMEVTS'] = str(args.events)
    myenv['NUMEVTSLOTS'] = str(args.evtSlots)
    myenv['NUMTHREADS'] = str(args.threads)
    myenv['TESTDBKEY']  = args.TestFileDBKey
    myenv['FTDECOVER']  = str(args.FTDecoVersion)
    myenv['FILE']  = ",".join(args.inputFileNames)
    myenv['OPTSFILE'] = args.OptsFileName

    runningJobs=[]

    tag = ''
    if args.outfileTag != '':
        tag = '_' + args.outfileTag + '_'

    for job in range(args.jobs):
        outputFileName = 'ThroughputTest{:s}.{:s}.{:d}t.{:d}j.{:d}e.{:d}'.format(tag, os.environ['CMTCONFIG'], args.threads, args.jobs, args.events, job)
        runningJobs.append(runJob(outputFileName, job, nbNumaNodes, myenv, args.profile))

    throughputs = []
    regex = re.compile("Evts\/s = ([\d.]+)")
    for idx, (job, ofile) in enumerate(runningJobs):
        retcode = job.wait()
        if retcode != 0 :
            print("WARNING: non-zero return code from job {} with output file {}".format(idx,ofile.name))
        # set file index to top of file
        ofile.seek(0)
        # read all lines from file
        lines = ofile.readlines();
        for l in lines[::-1]:
            tmp   = regex.search(l)
            if tmp:
                throughputs.append(float(tmp.group(1)))
                print("Throughput of job {} is {} Evts/s.".format(idx,throughputs[-1]))
                break

        ofile.close()

    print("Throughput test is finished. Overall reached Throughput is {} Evts/s".format(sum(throughputs)))

if __name__ == '__main__':
    sys.exit(main())
