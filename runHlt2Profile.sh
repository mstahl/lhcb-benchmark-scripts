#!/bin/bash

./cleanup.sh

python RunHlt2ThroughputJobs.py --outfileTag Profile -j 1 -n 1000 --FTDecoVersion 2 -t 1 -f /scratch/z5/data/Hlt2Throughput/minbias_filtered_49.mdf --forcerun --profile $1 UpgradeHLT1FilteredMinbias

python RunHlt2ThroughputJobs.py --outfileTag Hlt2Reco -j 40 -n 1000 --FTDecoVersion 2 -a Reco RecoTrBestSeq RecoDecodingSeq RecoTrFastSeq RecoRICHSeq RecoMUONSeq RecoCALOFUTURESeq -f /scratch/z5/data/Hlt2Throughput/*mdf --nonuma --forcerun -t1 $1 UpgradeHLT1FilteredMinbias 2>&1 | tee ThroughputTest_MaxThrough.log
