#!/usr/bin/env python
"""Runs a single job as configured via specified options file which is passed to SetupThroughputOpts.py """

import re
import os
import sys
import argparse
import subprocess
import csv


def runJob(OutputFileName, jobidx, nbNumaNodes, modifiedEnv, doprofile):
    '''Run a test with the given options'''

    # open log file
    outputFile = open("{}.log".format(OutputFileName), 'w+')

    # build command line
    cmdLine = ["gaudirun.py", str(os.path.dirname(os.path.realpath(__file__))) + "/SetupHlt2ThroughputOpts.py"]

    # deal with numa if needed
    if nbNumaNodes > 1:
        node = jobidx % nbNumaNodes
        cmdLine = ["numactl", "-N", str(node), "-m", str(node), "--"] + cmdLine

    # run the test
    print("Launching job with cmdLine: {}".format(cmdLine))
    process = subprocess.Popen(cmdLine, stdout=outputFile, stderr=subprocess.STDOUT, env=modifiedEnv)

    if doprofile:
     cmdLine = [str(os.path.dirname(os.path.realpath(__file__)))+"/doHlt2profile.sh" , str(process.pid), modifiedEnv['TAG']]
     if nbNumaNodes > 1:
       node = (jobidx % nbNumaNodes) + 1
       cmdLine = ["numactl", "-N", str(node), "-m", str(node), "--"] + cmdLine
     profprocess = subprocess.Popen(cmdLine)


    return process, outputFile

def getTrhoughputs(idx, tag, njobs, nevents, algs, throughputs_per_job):
  throughputs_per_event_and_job = {}
  for alg in algs:
    throughputs_per_event_and_job[alg] = []
  #hack here: the actual filename is specified in the runtime options called by SetupThroughputOpts
  with open('./time/Timeline_{:s}.{:s}.{:d}j.{:d}e.{:d}.csv'.format(tag,os.environ['CMTCONFIG'], njobs, nevents, idx)) as ofile :
    reader = csv.reader(ofile, delimiter=' ')
    next(reader)
    for l in reader:
      if l[2] not in algs : continue
      if int(l[5]) > 20: ## HARCODED (let 20 events run before calculating the throughput)
        throughputs_per_event_and_job[l[2]].append(1.e-9*(int(l[1]) - int(l[0]))) ## TimelineSvc output is in nanoseconds
    ofile.close()

  for alg in algs:
    try:
      throughputs_per_job[alg].append(len(throughputs_per_event_and_job[alg])/sum(throughputs_per_event_and_job[alg]))
    except:
      print("{0} not in input algorithms. check your options".format(alg))
  return


def main():
  '''Main method : parses options and calls runJob which configures and runs a single Brunel job'''

  parser = argparse.ArgumentParser(description=__doc__)

  parser.add_argument('-j', '--jobs', type=int, required=True,
                      help='nb of jobs to launch')

  parser.add_argument('-t', '--threads', type=int, required=True,
                      help='nb of threads per job')

  parser.add_argument('-e', '--evtSlots', type=int, default=-1,
                      help='nb of event slots per job (default: nb of threads per job + 10%%)')

  parser.add_argument('-n', '--events', default=1000, type=int,
                      help='nb of events to process per thread (default: 1000)')

  parser.add_argument('--outfileTag', type=str, default='',
                      help="Output file name tag, name will be 'ThroughputTest_TAG_...' .(default: '')")

  parser.add_argument('OptsFileName', type=str,
                      help='Option file name which defines the reconstruction sequence to run.')

  parser.add_argument('TestFileDBKey', type=str,
                      help='TestFileDB key name which defines input files and tags.')

  parser.add_argument('--FTDecoVersion', default=4, type=int,
                      help='SciFi Decoding Version must match used input files (default: 4)')

  parser.add_argument('-f','--inputFileNames', type=str, nargs='+', default="",
                      help='Names of input files, multiple names possible')

  parser.add_argument('-a','--algorithms', type=str, nargs='+', default=["Reco"],
                      help='Names of algorithms to measure throughputs. A throughput rate is calulated for the first in the list, all others are measured relative to that')

  parser.add_argument('--nonuma', action='store_true',
                      help='whether to disable usage of numa domains.')

  parser.add_argument('--profile', action='store_true',
                      help='whether to enable vtune profiling - only supported for single job.')

  parser.add_argument('--forcerun', action='store_true',
                      help='force running jobs, even if output files already exist')

  args = parser.parse_args()
  if args.evtSlots == -1:
      args.evtSlots = 1+int(11*args.threads/10)

  if args.profile and args.jobs >1:
      raise RuntimeError("VTune Profile only supported for single job")

  try:
      with open(os.devnull, 'w') as FNULL:
          subprocess.check_call(['amplxe-cl', '--help'], stdout=FNULL )
  except:
      if args.profile:
          raise RuntimeError("can't execute amplxe-cl, please make sure that you have intel tools setup correctly")
      else:
          print("Warning: can't execute amplxe-cl, please make sure that you have intel tools setup correctly")

  try:
      with open(os.devnull, 'w') as FNULL:
          subprocess.check_call(['gaudirun.py', '--help'], stdout=FNULL )
  except:
      raise RuntimeError("can't execute gaudirun.py, please make sure that you are in a Brunel environment")

  # deal with numa config
  nbNumaNodes = 1
  if not args.nonuma:
      try:
          output = subprocess.check_output(["numactl", "-show"])
          nodeline = [line for line in output.split('\n') if line.startswith('nodebind')][0]
          nbNumaNodes = len(nodeline.split()) - 1
          if(nbNumaNodes != args.jobs) and not args.profile:
              print("Warning: There are {} available numa nodes but you are launching {} jobs".format(nbNumaNodes, args.jobs))
      except:
          # numactl not existing
          print 'Warning : -- numactl not found, running without setting numa nodes --'


  # check how many threads the cpu actually has
  cputhreads = int(subprocess.check_output(["lscpu", "-e=CPU"]).split('\n')[-2]) + 1

  # check total number of threads
  if args.jobs*args.threads > cputhreads:
      print("CPU seems to only have {} threads but you specified {} jobs with {} threads each. This will overcommit the CPU, do you know what you are doing?".format(cputhreads, args.jobs, args.threads))

  myenv = os.environ.copy()
  myenv['NUMEVTS'] = str(args.events)
  myenv['NUMTHREADS'] = str(args.jobs)
  myenv['TESTDBKEY']  = args.TestFileDBKey
  myenv['FTDECOVER']  = str(args.FTDecoVersion)
  myenv['FILE']  = ",".join(args.inputFileNames)
  myenv['OPTSFILE'] = args.OptsFileName
  myenv['TAG'] = args.outfileTag

  runningJobs=[]

  skipjobs = False
  for job in range(args.jobs):
      myenv['NUMEVTSLOTS'] = str(job)
      outputFileName = './logs/ThroughputTest_{:s}.{:s}.{:d}t.{:d}j.{:d}e.{:d}'.format(args.outfileTag, os.environ['CMTCONFIG'], args.threads, args.jobs, args.events, job)
      timeFileName = './time/Timeline_{:s}.{:s}.{:d}j.{:d}e.{:d}.csv'.format(args.outfileTag,os.environ['CMTCONFIG'], args.jobs, args.events, job)
      if os.path.isfile(timeFileName) and not args.forcerun :
        if job == 0:
          print('Output files of this job already exist. Run with --forcerun to re-run job')
          skipjobs = True
      else :
        if job == 0:
          if not os.path.exists("./logs"):
            os.mkdir("./logs")
          if not os.path.exists("./time"):
            os.mkdir("./time")
        runningJobs.append(runJob(outputFileName, job, nbNumaNodes, myenv, args.profile))

  # the following lines are for calculating the throughput in the old configuration framework
  if not args.profile: #skip calculation when doing a profile
    throughputs = {}
    for alg in args.algorithms:
      throughputs[alg] = []
    if skipjobs:
      for idx in range(args.jobs):
        getTrhoughputs(idx,args.outfileTag,args.jobs,args.events,args.algorithms,throughputs)
    else:
      for idx, (job,dummyofn) in enumerate(runningJobs):
        retcode = job.wait()
        if retcode != 0 :
            print("WARNING: non-zero return code from job {}".format(idx))
        getTrhoughputs(idx,args.outfileTag,args.jobs,args.events,args.algorithms,throughputs)

    #print table calculating how many percent each algorithm is taking up
    sum_main_tp = sum(throughputs[args.algorithms[0]])
    average_main_tp = sum_main_tp/len(throughputs[args.algorithms[0]])
    #The following line will be picked up by the ThroughputProfileHandler
    print("Throughput test is finished. Combined throughput for {0}: Evts/s = {1:5.2f} ( {2:5.2f} per node )".format(args.algorithms[0],sum_main_tp,average_main_tp))
    for k, v in throughputs.iteritems():
      if k == args.algorithms[0] : continue
      try:
        print("Relative consumption of {0:<24} w.r.t. {1} is {2:5.2f} %".format(k,args.algorithms[0], 100*average_main_tp/(sum(v)/len(v))))
      except:
        print("{0} not in input algorithms. check your options".format(k))

if __name__ == '__main__':
    sys.exit(main())
