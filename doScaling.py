#!/usr/bin/env python
"""Runs a various thread : job  configurations for scaling test"""

import re
import os
import sys
import argparse
import subprocess

DEFAULT_CONFIG=("1:2:4:50000,2:2:4:50000,4:2:4:50000,10:2:4:50000,18:2:4:100000,20:2:4:100000,22:2:4:100000,"
                "1:8:10:200000,2:8:10:200000,3:8:10:200000,4:8:10:400000,5:8:10:400000,6:8:10:400000,"
                "1:10:12:250000,2:10:12:250000,3:10:12:250000,"
                "1:18:20:400000,2:18:20:400000,"
                "1:20:24:500000,2:20:24:500000,"
                "1:22:28:600000,2:22:24:600000")


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-c','--config', type=str, default=DEFAULT_CONFIG,
            help='''nb of configs to run test for specified as a list:
            "j:t:e:n,j:t:e:n,j:t:e:n,... where j=job, t=threads, e=Eventslots and n=number of events per job''')

    parser.add_argument('OptsFileName', type=str,
                        help='Option file name which defines the reconstruction sequence to run.')

    parser.add_argument('TestFileDBKey', type=str,
                        help='TestFileDB key name which defines input files and tags.')

    parser.add_argument('--FTDecoVersion', default=4, type=int,
                        help='SciFi Decoding Version must match used input files')

    parser.add_argument('-f','--inputFileNames', type=str, nargs='+', default="",
                        help='Names of input files, multiple names possible')


    args = parser.parse_args()

    configs = args.config.split(',')
    for conf in configs:
        jobs, threads, eventslots, events = conf.split(':')

        cmdLine = ['python', './RunThroughputJobs.py','--outfileTag','Scaling', '-e', eventslots, '-t', threads,
                    '-j', jobs, '-n', events, args.OptsFileName, args.TestFileDBKey, '-f'] + args.inputFileNames

        print("Starting config " + conf)
        process  = subprocess.Popen(cmdLine)
        process.wait()

if __name__ == '__main__':
    sys.exit(main())
