#! /usr/bin/bash

set -euo pipefail

echo "yeah yeah yeah yeah such profile much wow! "
sleep 100
amplxe-cl -collect hotspots -d 180 -target-pid=${1} -r profile_out > /dev/null

amplxe-cl -R top-down -column="CPU Time:Self","Module" -report-out result.csv -format csv -csv-delimiter comma -r profile_out
sed -i '1d' result.csv > /dev/null

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $dir
$dir/stackcollapse-vtune.pl result.csv | $dir/flamegraph.pl --cp --title "Hlt2 Flame Graph" --minwidth 2 --width 1600 > flamy.svg

echo "Done"
