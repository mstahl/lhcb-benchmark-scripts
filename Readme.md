This repository provides a set of scripts to ease benchmarking of Minibrunel.
You will find there 7 scripts and here is a short description of each of them

The first one to use is RunThroughputJobs.py, which runs a throughput test given a certain options file.
Note that this is only supported for options files which are part of `Brunel/Rec/Brunel/python/upgrade_options`    

Check the help, but main things you want to know are :
  - OptsFileName specifies the options file to be used from `Brunel/Rec/Brunel/python/upgrade_options`, only name without .py ending
  - TestFileDBKey sets the input file one wishes to use, for actual testing please overwrite filepath to use local files see -f option.
  - -j Defines the number of independent jobs you want to launch. Following options are per Job!
  - -t allows to give the number of threads you want to use, for example -t 4 will use 4 threads for each job
  - -e allows to change number of eventslots per job
  - -n allows to change number of events per job
  - --FTDecoVersion allows you to specify the needed decoding version for the FTDecoding
  - --nonuma disables numa handling, if you don't know what that means, please leave it on (default).
  - -f specify filepaths of the input files matching the TestFileDBKey
  - --profile this will automatically use vtune to profile a single job (no multijob support) and produce a flamegraph of the result

Take care when you run to use as input a file in RAMFS if you do not want to be slowed down by IO.

The output of `RunThroughputJobs.py` is a set of .log files. One of each per job. They will appear in the current directory and are named after the parameters of the test.

`SetupThroughputOpts.py`, `doprofile.sh`, `flamegraph.pl`, and `stackcollapse-vtune.pl` are helper scripts used internally in `RunThroughputJobs.py`, don't worry about it.

`doScaling.py` is a script that enables you to easily run the `RunThroughputJobs.py` for different number of jobs,threads,eventslots, and number of events. The options are similar to the `RunThroughputJobs.py` and are also documented in `./doScaling.py --help`.

`plotScaling.py` is simply executed in the base folder and looks for files matching the naming convention of the produced log files of the `doScaling.py` test and then produces a plot `scalingTest.png` which shows the throughput of each tested configuration.

