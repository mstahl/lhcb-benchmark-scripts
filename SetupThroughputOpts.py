import os, importlib
from Gaudi.Configuration import importOptions

num_events     = int(os.environ['NUMEVTS'])
num_eventslots = int(os.environ['NUMEVTSLOTS'])
num_threads    = int(os.environ['NUMTHREADS'])
testDBKey      = str(os.environ['TESTDBKEY'])
FTDecoVer      = int(os.environ['FTDECOVER'])
filepath       = os.environ['FILE'].split(',')
optsfile       = str(os.environ['OPTSFILE'])

opts=importlib.import_module("upgrade_options.{}".format(optsfile))

# fix for empty list coming from the env variables
if filepath == ['']:
    filepath = []

print("###SETUPTHROUGHPUTOPTS###\nImporting {} and executing with numevents:{}, eventslots:{}, threads:{} , using TestFileDBKey {}, SciFi decoding version {}, and set files to: {}".format(optsfile,num_events,num_eventslots, num_threads, testDBKey, FTDecoVer, filepath))

opts.runTest(testDBKey, nbEventSlots=num_eventslots, threadPoolSize=num_threads, evtMax=num_events, FTDecoVersion=FTDecoVer, filepaths=filepath)
